package com.example.apple

import android.provider.ContactsContract
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiApple {

    @GET("v2/brands/apple-phones-48?page=2")
    fun getllista(): Call<Data>

//    @GET("http://api-mobilespecs.azharimm.site/v2/apple_ipad_9_7_(2018)-9142")
//    fun getdetails(): Call<ContactsContract.Contacts.Data>

    companion object {
        val BASE_URL = "https://api-mobilespecs.azharimm.site/"
        fun create(): ApiApple {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiApple::class.java)
        }
    }
}
