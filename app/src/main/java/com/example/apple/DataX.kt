package com.example.apple

data class DataX(
    val current_page: Int,
    val last_page: Int,
    val phones: List<Phone>,
    val title: String
)