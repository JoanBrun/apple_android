package com.example.apple

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class ListAdapter (items: List<Phone>):
    RecyclerView.Adapter<ListAdapter.ListViewHolder>() {

    private val items = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.mobile, parent, false)

        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bindData(items[position])
        holder.itemView.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_listMobileFragment_to_mobileDescription)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var title: TextView
        private var image: ImageView

        init {
            title = itemView.findViewById(R.id.nom)
            image = itemView.findViewById(R.id.imageView)
        }

        fun bindData(items: Phone) {
            title.text = items.phone_name
            Picasso.get().load(items.image).into(image)
        }
    }
}