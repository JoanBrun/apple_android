package com.example.apple

import android.os.Bundle
import android.os.Handler
import android.telecom.Call
import android.util.Log
import android.view.View
import android.widget.GridLayout
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import javax.security.auth.callback.Callback

class ListMobileFragment : Fragment(R.layout.mobiles_fragment){
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewModel: ListViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ListViewModel::class.java)

        recyclerView = view.findViewById(R.id.recycler_view)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        if (viewModel.ft == 1){
            recyclerView.adapter = ListAdapter(viewModel.getItems())
        }else {
            Handler().postDelayed({
                recyclerView.adapter = ListAdapter(viewModel.getItems())
                viewModel.ft = 1
            }, 1000)
        }
        Handler().postDelayed({
            recyclerView.adapter = ListAdapter(viewModel.getItems())
        }, 1000)
    }
}