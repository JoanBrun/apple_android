package com.example.apple

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListViewModel (application: Application): AndroidViewModel(application) {
    private var data: Data? = null

    init {
        loadData()
    }

    var ft = 0
    fun getItems(): List<Phone> {
        return data?.data?.phones!!
    }

    fun loadData(){
        val call = ApiApple.create().getllista()
        call.enqueue(object: Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Data>, response: Response<Data?>) {
                if (response.isSuccessful) {
                    data = response.body()!!
                }
            }
        })
    }

}