package com.example.apple

import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener


class MobileDescription: Fragment(R.layout.mobile_details) {
    var sampleImages = arrayOf(
        "https://fdn2.gsmarena.com/vv/pics/apple/apple-ipad-97-2018-1.jpg",
        "https://fdn2.gsmarena.com/vv/pics/apple/apple-ipad-97-2018-2.jpg",
        "https://fdn2.gsmarena.com/vv/pics/apple/apple-ipad-97-2018-3.jpg"
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val carouselView = view.findViewById(R.id.carouselView) as CarouselView;
        carouselView.setImageListener(imageListener);
        carouselView.setPageCount(sampleImages.size);
    }

    var imageListener: ImageListener = object : ImageListener {
        override fun setImageForPosition(position: Int, imageView: ImageView) {
            Picasso.get().load(sampleImages[position]).into(imageView)
        }
    }
}